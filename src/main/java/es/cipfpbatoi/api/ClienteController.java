package es.cipfpbatoi.api;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clientes")
public class ClienteController {
	@Autowired
	ClienteService clienteService;

	@GetMapping("")
	public List<Cliente> list() {
		return clienteService.listAllClientes();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Cliente> get(@PathVariable Integer id) {
		try {
			Cliente cli = clienteService.getCliente(id);
			return new ResponseEntity<Cliente>(cli, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("")
	public ResponseEntity<?> add(@RequestBody Cliente cli) {
		Cliente cliNuevo = clienteService.saveCliente(cli);
		return new ResponseEntity<>(cliNuevo.getId(), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Cliente cli, @PathVariable Integer id) {
		try {
			Cliente actualCli = clienteService.getCliente(id);
			actualCli.setNombre(cli.getNombre());
			actualCli.setDireccion(cli.getDireccion());
			clienteService.saveCliente(actualCli);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Integer id) {
		try {
			clienteService.deleteCliente(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (EmptyResultDataAccessException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
