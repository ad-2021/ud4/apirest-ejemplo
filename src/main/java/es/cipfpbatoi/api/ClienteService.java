package es.cipfpbatoi.api;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ClienteService {
	@Autowired
	private ClienteRepository clienteRespository;

	public List<Cliente> listAllClientes() {
		return clienteRespository.findAll();
	}

	public Cliente getCliente(Integer id) {
		return clienteRespository.findById(id).get();
	}

	public Cliente saveCliente(Cliente cli) {
		cli = clienteRespository.save(cli);
		return cli;
//		System.out.println(cli);
	}
	
	public void deleteCliente(Integer id) {
		clienteRespository.deleteById(id);
	}

}
